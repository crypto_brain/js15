/*
 *  Document   : base_ui_activity.js
 *  Author     : pixelcave
 *  Description: Custom JS code used in Activity Page
 */

var BaseUIActivity = function() {
    // Randomize progress bars values
    var barsRandomize = function(){
        jQuery('.js-bar-randomize').on('click', function(){
            jQuery(this)
                .parents('.block')
                .find('.progress-bar')
                .each(function() {
                    var $this   = jQuery(this);
                    var $random = Math.floor((Math.random() * 91) + 10)  + '%';

                    $this.css('width', $random);

                    if ( ! $this.parent().hasClass('progress-mini')) {
                        $this.html($random);
                    }
                });
            });
    };

    // SweetAlert, for more examples you can check out https://github.com/t4t5/sweetalert
    var sweetAlert = function(){
        // Init a simple alert on button click
        jQuery('.js-swal-alert').on('click', function(){
            swal('Hi, this is a simple alert!');
        });
        
        // Init a bid alert on button click
        jQuery('.js-swal-bid').on('click', function(){
            swal({
                title: 'Estamos enviando sua oferta para as Corretoras na sua região!',
                text: 'Sua oferta será respondida em menos de 1 minuto.',
                type: 'warning',
                confirmButtonColor: '#70b9eb',
                confirmButtonText: 'Ver ofertas',
                closeOnConfirm: false,
                html: false
            
            });
        });
        
        // Init a broker accept alert on button click
        jQuery('.js-swal-bid').on('click', function(){
            swal({
       title: 'Estamos enviando seu lance para as corretoras de câmbio',
                text: 'Você receberá respostas em menos de 1 minuto..',
                confirmButtonColor: '#70b9eb',
                confirmButtonText: 'Ver qual corretora aceitou seu lance',
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
                html: false
            },
            function(){
                setTimeout(function(){
                swal({
                title: '<img class="img-avatar" src="assets/img/avatars/avatar13.jpg" alt=""><h3 class="" style="margin-top:20px;">Confidence DTVM: 12.123.1231/0001-23</h3><p>Avaliação: <i class="fa fa-star text-warning"></i><i class="fa fa-star text-warning"></i><i class="fa fa-star text-warning"></i><i class="fa fa-star text-warning"></i><i class="fa fa-star text-warning"></i></p>',
                text: '<span>Entrega em 1 dia útil <i class="fa fa-motorcycle text-black" alt="entrega" data-toggle="tooltip" data-placement="right" title="Entrega no seu local com seguro garantido."></i></span> ou Retirar no local <img src="assets/img/avatars/uber.jpg" alt="Uber" data-toggle="tooltip" data-placement="right" title="Vá retirar seu dinheiro na corretora de UBER. No seu email de aprovação você encontrará o botão para ir de UBER." /><p><h2 class="font-w300"> Valor da taxa: <strong>R$3,12</strong></h2></p><p><h2 class="font-w300"> Economize: <strong>R$250,00</strong></h2></p><p class="h6"><i class="fa fa-info-circle"></i> Você consegue finalizar essa ordem em menos de 5 minutos.</p>',
                
                confirmButtonColor: '#44b4a6',
                confirmButtonText: 'Ir para Pagamento',
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
                html: true
               
                });
            }, 20);
            
            });
        });

        // Init an success alert on button click
        jQuery('.js-swal-success').on('click', function(){
            swal('Success', 'Everything updated perfectly!', 'success');
        });

        // Init an error alert on button click
        jQuery('.js-swal-error').on('click', function(){
            swal('Oops...', 'Something went wrong!', 'error');
        });

        // Init an example confirm alert on button click
        jQuery('.js-swal-confirm').on('click', function(){
            swal({
                title: 'Are you sure?',
                text: 'You will not be able to recover this imaginary file!',
                showCancelButton: true,
                confirmButtonColor: '#d26a5c',
                confirmButtonText: 'Yes, delete it!',
                closeOnConfirm: false,
                html: false
            }, function () {
                swal('Deleted!', 'Your imaginary file has been deleted.', 'success');
            });
        });
    };

    return {
        init: function() {
            // Init randomize bar values
            barsRandomize();

            // Init SweetAlert
            sweetAlert();
        }
    };
}();

// Initialize when page loads
jQuery(function(){ BaseUIActivity.init(); });